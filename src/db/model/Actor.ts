import { DataTypes, Model } from 'sequelize'
import sequelize from '@/db'

class Actor extends Model { }

Actor.init({
    actor_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING
}, {
    sequelize,
    modelName: 'Actor',
    tableName: 'actor',
    timestamps: false
})

export default Actor