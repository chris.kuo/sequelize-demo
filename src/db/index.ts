import { Sequelize } from 'sequelize'

const sequelize = new Sequelize('sakila', 'db_admin', 'beyondcars123', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        freezeTableName: true
    }
})

export default sequelize