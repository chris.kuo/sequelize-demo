import type { NextApiRequest, NextApiResponse } from 'next'
import { Op } from 'sequelize'
import Actor from '@/db/model/Actor'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {

  // INSERT 
  // const actor = await Actor.create({
  //   first_name: 'chris',
  //   last_name: 'kuo'
  // })

  // SELECT * FROM 
  // const actors = await Actor.findAll();

  // SELECT first_name, last_name FROM ... 
  // const actors = await Actor.findAll({
  //   attributes: ['first_name', 'last_name']
  // });

  // SELECT 
  // const actors = await Actor.findAll({
  //   attributes: { exclude: ['first_name'] }
  // })

  // AND 
  // const actors = await Actor.findAll({
  //   where: {
  //     first_name: 'chris',
  //     last_name: 'kuo'
  //   }
  // })

  // Op AND
  // const actors = await Actor.findAll({
  //   where: {
  //     [Op.and]: [
  //       { first_name: 'chris' },
  //       { last_name: 'kuo'}
  //     ]
  //   }
  // })

  // Op OR
  // const actors = await Actor.findAll({
  //   where: {
  //     [Op.or]: [
  //       { first_name: 'chris' },
  //       { first_name: 'neg-orange'}
  //     ]
  //   }
  // })

  // UPDATE. return number
  // const actor = await Actor.update({ first_name: 'orange' }, {
  //   where: {
  //     last_name: 'kuo'
  //   }
  // })

  // creating multiple records at once
  // const lovers = await Actor.bulkCreate([
  //   { first_name: 'neg-orange', last_name: 'lin'},
  //   { first_name: 'neg-tina', last_name: 'lee'},
  //   { first_name: 'ziggurat', last_name: 'chen'}
  // ])

  // const actor = await Actor.findByPk(212);

  // const actor = await Actor.findOne({
  //   where: {
  //     first_name: 'neg-orange'
  //   }
  // });

  // order desc/asc
  const actors = await Actor.findAll({
    order: [
      ['first_name', 'desc']
    ]
  })

  res.status(200).json( actors )
}
